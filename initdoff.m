function initdoff(m1f,m2f)
%initdoff -- add to helmprb the values of the fields
%
%    [helmprb.begin(idx), helmprb.dofend(idx)) point to dofs of face idx
%
%    helmprb.ndoff(idx) is the ndof of face idx
%
%    helmprb.m1(idof), helmprb.m2(idof) represent the mode
%    cos(m1*t1)*cos(m2*t2) for the dof with global index idof

global helmprb;
helmprb.dofend= zeros(helmprb.tnf,1);
helmprb.dofbegin= zeros(helmprb.tnf,1);
helmprb.ndoff= zeros(helmprb.tnf,1);

%   constant number of dof per face
% default 4 modes
if nargin<2
    helmprb.ndoff(:)= 4;
    m1f= [0 0 1 1].'; m2f= [0 1 0 1].';
elseif numel(m1f)~=numel(m2f)
    error('numel of m1f and m2f are not equal');
else
    helmprb.ndoff(:)= numel(m1f);
    m12= [m1f(:), m2f(:)];
    m12= unique(m12,'rows');
    m1f= m12(:,1); m2f= m12(:,2);
end
% base index 1
helmprb.dofend= helmprb.ndoff(1)*(1:helmprb.tnf).'+1;
helmprb.dofbegin(1)= 1;
helmprb.dofbegin(2:end)= helmprb.dofend(1:end-1);
helmprb.m1= m1f(:,ones(helmprb.tnf,1));
helmprb.m2= m2f(:,ones(helmprb.tnf,1));

%   describe ndof and dofs on every face
% $$$ n= helmprb.npml;
% $$$ nf= helmprb.nf;
% $$$ for dire= 1:3
% $$$     for ix= 1:n(1)+(dire==1)
% $$$         for iy= 1:n(2)+(dire==2)
% $$$             for iz= 1:n(3)+(dire==3)
% $$$                 idx= sub2idx(dire,[ix,iy,iz],n,nf);
% $$$                 helmprb.ndoff(idx)= 4;
% $$$                 helmprb.
% $$$             end
% $$$         end
% $$$     end
% $$$ end