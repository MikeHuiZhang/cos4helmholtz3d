function u = evalsol(helmprb,un,x,y,z)
%evalsol.m -- Evaluate values of the solution at given mesh points.
% 
% x,y,z -- 3D arrays to represent 3D subscripts in a cartesian mesh, obtained
% by meshgrid

%   initialization
h= helmprb.h;
u= zeros(size(x));

%   homogeneous part: loop over every physical element
for iele= 1:prod(helmprb.n)
    %   3D subscripts of the element
    [iz iy ix]= ind2sub(fliplr(helmprb.n.'),iele);    
    subpml= [ix;iy;iz] + helmprb.eta([1 3 5]).';
    
    %   wavenumber, density of this element
    wele= helmprb.w(ix,iy,iz);
    rhoele= helmprb.rho(ix,iy,iz);

    %   reference coordinates of evaluation points in the reference element
    %   [0,h]
    x1d= x(1,:,1)-helmprb.domain(1,1);
    y1d= y(:,1,1)-helmprb.domain(2,1);
    z1d= z(1,1,:)-helmprb.domain(3,1);
    inx= (ix-1)*h(1)<=x1d & (x1d<ix*h(1) | x1d==helmprb.domain(1,2)-helmprb.domain(1,1)); inx= inx(:);
    iny= (iy-1)*h(2)<=y1d & (y1d<iy*h(2) | y1d==helmprb.domain(2,2)-helmprb.domain(2,1)); iny= iny(:);
    inz= (iz-1)*h(3)<=z1d & (z1d<iz*h(3) | z1d==helmprb.domain(3,2)-helmprb.domain(3,1)); inz= inz(:);
    xx= x(iny,inx,inz)-helmprb.domain(1,1)-(ix-1)*h(1); 
    yy= y(iny,inx,inz)-helmprb.domain(2,1)-(iy-1)*h(2); 
    zz= z(iny,inx,inz)-helmprb.domain(3,1)-(iz-1)*h(3);
    
    %-{ contribution from Neumann on the left face
    idx= sub2idx(1,subpml,helmprb.npml,helmprb.nf);
    for idof= helmprb.dofbegin(idx):helmprb.dofend(idx)-1
        ky= helmprb.m1(idof)*pi/h(2);
        kz= helmprb.m2(idof)*pi/h(3);
        s= sqrt(ky^2+kz^2-wele^2);
        u(iny,inx,inz)= u(iny,inx,inz) - rhoele/s*un(idof)*cos(kz*zz).*cos(ky*yy).*(exp(-s*(xx-h(1)))+exp(s*(xx-h(1))))./(-exp(-s*h(1))+exp(s*h(1)));
    end
    %-}
    
    %-{ contribution from Neumann on the right face
    idx= sub2idx(1,subpml+[1;0;0],helmprb.npml,helmprb.nf);
    for idof= helmprb.dofbegin(idx):helmprb.dofend(idx)-1
        ky= helmprb.m1(idof)*pi/h(2);
        kz= helmprb.m2(idof)*pi/h(3);
        s= sqrt(ky^2+kz^2-wele^2);
        u(iny,inx,inz)= u(iny,inx,inz) + rhoele/s*un(idof)*cos(kz*zz).*cos(ky*yy).*(exp(-s*xx)+exp(s*xx))./(-exp(-s*h(1))+exp(s*h(1)));
    end
    %-}
    
    %-{ contribution from Neumann on the front face
    idx= sub2idx(2,subpml,helmprb.npml,helmprb.nf);
    for idof= helmprb.dofbegin(idx):helmprb.dofend(idx)-1
        kx= helmprb.m1(idof)*pi/h(1);
        kz= helmprb.m2(idof)*pi/h(3);
        s= sqrt(kx^2+kz^2-wele^2);
        u(iny,inx,inz)= u(iny,inx,inz) - rhoele/s*un(idof)*cos(kz*zz).*cos(kx*xx).*(exp(-s*(yy-h(2)))+exp(s*(yy-h(2))))./(-exp(-s*h(2))+exp(s*h(2)));
    end
    %-}

    %-{ contribution from Neumann on the back face
    idx= sub2idx(2,subpml+[0;1;0],helmprb.npml,helmprb.nf);
    for idof= helmprb.dofbegin(idx):helmprb.dofend(idx)-1
        kx= helmprb.m1(idof)*pi/h(1);
        kz= helmprb.m2(idof)*pi/h(3);
        s= sqrt(kx^2+kz^2-wele^2);
        u(iny,inx,inz)= u(iny,inx,inz) + rhoele/s*un(idof)*cos(kz*zz).*cos(kx*xx).*(exp(-s*yy)+exp(s*yy))./(-exp(-s*h(2))+exp(s*h(2)));
    end
    %-}

    %-{ contribution from Neumann on the bottom face
    idx= sub2idx(3,subpml,helmprb.npml,helmprb.nf);
    for idof= helmprb.dofbegin(idx):helmprb.dofend(idx)-1
        kx= helmprb.m1(idof)*pi/h(1);
        ky= helmprb.m2(idof)*pi/h(2);
        s= sqrt(kx^2+ky^2-wele^2);
        u(iny,inx,inz)= u(iny,inx,inz) - rhoele/s*un(idof)*cos(ky*yy).*cos(kx*xx).*(exp(-s*(zz-h(3)))+exp(s*(zz-h(3))))./(-exp(-s*h(3))+exp(s*h(3)));
    end
    %-}

    %-{ contribution from Neumann on the top face
    idx= sub2idx(3,subpml+[0;0;1],helmprb.npml,helmprb.nf);
    for idof= helmprb.dofbegin(idx):helmprb.dofend(idx)-1
        kx= helmprb.m1(idof)*pi/h(1);
        ky= helmprb.m2(idof)*pi/h(2);
        s= sqrt(kx^2+ky^2-wele^2);
        u(iny,inx,inz)= u(iny,inx,inz) + rhoele/s*un(idof)*cos(ky*yy).*cos(kx*xx).*(exp(-s*zz)+exp(s*zz))./(-exp(-s*h(3))+exp(s*h(3)));
    end
    %-}
    
end

%   inhomogeneous part: loop over every point source
for isrc= 1:size(helmprb.scpoints,1)
    %   strength of this point source
    Sp= helmprb.Sp(isrc);
    
    %   3D subscripts of the element, base index 1
    subphy= 1+floor((helmprb.scpoints(isrc,:).'-helmprb.domain(:,1))./h);
    subphy= min(subphy,helmprb.n);
    subpml= subphy + helmprb.eta([1 3 5]).';

    %   wavenumber of this element
    wele= helmprb.w(subphy(1),subphy(2),subphy(3));

    %   reference coordinates of this point source in the reference element [0,h]
    r0= helmprb.scpoints(isrc,:)-helmprb.domain(:,1).'-((subphy-1).*h).';
    
    %   reference coordinates of evaluation points in the reference element
    x1d= x(1,:,1)-helmprb.domain(1,1);
    y1d= y(:,1,1)-helmprb.domain(2,1);
    z1d= z(1,1,:)-helmprb.domain(3,1);
    inx= (ix-1)*h(1)<=x1d & (x1d<ix*h(1) | x1d==helmprb.domain(1,2)-helmprb.domain(1,1)); inx= inx(:);
    iny= (iy-1)*h(2)<=y1d & (y1d<iy*h(2) | y1d==helmprb.domain(2,2)-helmprb.domain(2,1)); iny= iny(:);
    inz= (iz-1)*h(3)<=z1d & (z1d<iz*h(3) | z1d==helmprb.domain(3,2)-helmprb.domain(3,1)); inz= inz(:);
    xx= x(iny,inx,inz)-helmprb.domain(1,1)-(ix-1)*h(1); 
    yy= y(iny,inx,inz)-helmprb.domain(2,1)-(iy-1)*h(2); 
    zz= z(iny,inx,inz)-helmprb.domain(3,1)-(iz-1)*h(3);
    
    %-{ special solution in the left part
    idx= sub2idx(1,subpml,helmprb.npml,helmprb.nf);
    i1= xx(1,:,1)<=r0(1); i1= i1(:); inx1= false(size(inx)); inx1(inx)= i1;
    for idof= helmprb.dofbegin(idx):helmprb.dofend(idx)-1
        ky= helmprb.m1(idof)*pi/h(2);
        kz= helmprb.m2(idof)*pi/h(3);
        s= sqrt(ky^2+kz^2-wele^2);
        c= Sp/( 0.25*(1+(ky==0))*(1+(kz==0)) )/h(2)/h(3);
        a= c/2.0/s*(exp(-s*(r0(1)-h(1))) + exp(s*(r0(1)-h(1))))/(exp(s*h(1))-exp(-s*h(1)))*cos(ky*r0(2))*cos(kz*r0(3));
        % contribution to the average, weight 1.0/3.0 
        u(iny,inx1,inz)= u(iny,inx1,inz) + 1.0/3.0*a*(exp(-s*xx(:,i1,:)) + exp(s*xx(:,i1,:))).*cos(ky*yy(:,i1,:)).*cos(kz*zz(:,i1,:));
    end
    %-}
    
    %-{ special solution in the right part
    idx= sub2idx(1,subpml+[1;0;0],helmprb.npml,helmprb.nf);
    i1= ~i1; inx1= false(size(inx)); inx1(inx)= i1;
    for idof= helmprb.dofbegin(idx):helmprb.dofend(idx)-1
        ky= helmprb.m1(idof)*pi/h(2);
        kz= helmprb.m2(idof)*pi/h(3);
        s= sqrt(ky^2+kz^2-wele^2);
        c= Sp/( 0.25*(1+(ky==0))*(1+(kz==0)) )/h(2)/h(3);
        a= c/2.0/s*(exp(-s*r0(1))+exp(s*r0(1)))/(exp(s*h(1))-exp(-s*h(1)))*cos(ky*r0(2))*cos(kz*r0(3)); 
        % contribution to the average, weight 1.0/3.0 
        u(iny,inx1,inz)= u(iny,inx1,inz) + 1.0/3.0*a*(exp(-s*(xx(:,i1,:)-h(1))) + exp(s*(xx(:,i1,:)-h(1)))).*cos(ky*yy(:,i1,:)).*cos(kz*zz(:,i1,:));
    end
    %-}
    
    %-{ special solution in the front part
    idx= sub2idx(2,subpml,helmprb.npml,helmprb.nf);
    i1= yy(:,1,1)<=r0(2); i1= i1(:); iny1= false(size(iny)); iny1(iny)= i1;
    for idof= helmprb.dofbegin(idx):helmprb.dofend(idx)-1
        kx= helmprb.m1(idof)*pi/h(1);
        kz= helmprb.m2(idof)*pi/h(3);
        s= sqrt(kx^2+kz^2-wele^2);
        c= Sp/( 0.25*(1+(kx==0))*(1+(kz==0)) )/h(1)/h(3);
        a= c/2.0/s*(exp(-s*(r0(2)-h(2))) + exp(s*(r0(2)-h(2))))/(exp(s*h(2))-exp(-s*h(2)))*cos(kx*r0(1))*cos(kz*r0(3));
        % contribution to the average, weight 1.0/3.0 
        u(iny1,inx,inz)= u(iny1,inx,inz) + 1.0/3.0*a*(exp(-s*yy(i1,:,:)) + exp(s*yy(i1,:,:))).*cos(kx*xx(i1,:,:)).*cos(kz*zz(i1,:,:));
    end
    %-}
    
    %-{ special solution in the back part
    idx= sub2idx(2,subpml+[0;1;0],helmprb.npml,helmprb.nf);
    i1= ~i1; iny1= false(size(iny)); iny1(iny)= i1;
    for idof= helmprb.dofbegin(idx):helmprb.dofend(idx)-1
        kx= helmprb.m1(idof)*pi/h(1);
        kz= helmprb.m2(idof)*pi/h(3);
        s= sqrt(kx^2+kz^2-wele^2);
        c= Sp/( 0.25*(1+(kx==0))*(1+(kz==0)) )/h(1)/h(3);
        a= c/2.0/s*(exp(-s*r0(2))+exp(s*r0(2)))/(exp(s*h(2))-exp(-s*h(2)))*cos(kx*r0(1))*cos(kz*r0(3)); 
        % contribution to the average, weight 1.0/3.0 
        u(iny1,inx,inz)= u(iny1,inx,inz) + 1.0/3.0*a*(exp(-s*(yy(i1,:,:)-h(2))) + exp(s*(yy(i1,:,:)-h(2)))).*cos(kx*xx(i1,:,:)).*cos(kz*zz(i1,:,:));
    end
    %-}
    
    %-{ special solution in the bottom part
    idx= sub2idx(3,subpml,helmprb.npml,helmprb.nf);
    i1= zz(1,1,:)<=r0(3); i1= i1(:); inz1= false(size(inz)); inz1(inz)= i1;
    for idof= helmprb.dofbegin(idx):helmprb.dofend(idx)-1
        kx= helmprb.m1(idof)*pi/h(1);
        ky= helmprb.m2(idof)*pi/h(2);
        s= sqrt(kx^2+ky^2-wele^2);
        c= Sp/( 0.25*(1+(kx==0))*(1+(ky==0)) )/h(1)/h(2);
        a= c/2.0/s*(exp(-s*(r0(3)-h(3))) + exp(s*(r0(3)-h(3))))/(exp(s*h(3))-exp(-s*h(3)))*cos(kx*r0(1))*cos(ky*r0(2));
        % contribution to the average, weight 1.0/3.0 
        u(iny,inx,inz1)= u(iny,inx,inz1) + 1.0/3.0*a*(exp(-s*zz(:,:,i1)) + exp(s*zz(:,:,i1))).*cos(kx*xx(:,:,i1)).*cos(ky*yy(:,:,i1));
    end
    %-}

    %-{ special solution in the top part
    idx= sub2idx(3,subpml+[0;0;1],helmprb.npml,helmprb.nf);
    i1= ~i1; inz1(inz)= i1;
    for idof= helmprb.dofbegin(idx):helmprb.dofend(idx)-1
        kx= helmprb.m1(idof)*pi/h(1);
        ky= helmprb.m2(idof)*pi/h(2);
        s= sqrt(kx^2+ky^2-wele^2);
        c= Sp/( 0.25*(1+(kx==0))*(1+(ky==0)) )/h(1)/h(2);
        a= c/2.0/s*(exp(-s*r0(3))+exp(s*r0(3)))/(exp(s*h(3))-exp(-s*h(3)))*cos(kx*r0(1))*cos(ky*r0(2)); 
        % contribution to the average, weight 1.0/3.0 
        u(iny,inx,inz1)= u(iny,inx,inz1) + 1.0/3.0*a*(exp(-s*(zz(:,:,i1)-h(3))) + exp(s*(zz(:,:,i1)-h(3)))).*cos(kx*xx(:,:,i1)).*cos(ky*yy(:,:,i1));
    end
    %-}
    
end
