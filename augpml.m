function npml = augpml(n,eta)
%   augment PML    
npml= n;
npml(1)= npml(1)+eta(1)+eta(2);
npml(2)= npml(2)+eta(3)+eta(4);
npml(3)= npml(3)+eta(5)+eta(6);
