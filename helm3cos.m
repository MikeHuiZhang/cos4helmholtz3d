%helm3cos.m -- main program
%
% The Helmholtz problem in a cube:
%
%  rho div(1/rho grad u) + w^2 u = -S, in Omega= (xl,xr)X(yl,yr)X(zl,zr)
%
%                              u = 0,  Dirichlet faces, type 2
%
%               (1/rho Dn + T) u = 0,  Robin-like b.c., type 1
%
%                     1/rho Dn u = 0,  Neumann b.c., type 0
%
%
% where w= 2*pi*f/c.  We assume rho and c are constant on each element,
% otherwise we average them to let them be.  We use e.g.
%
%       bctype = [1 2 0 2 1 1]
%
% to describe the b.c. on the left, right, front, back, bottom and top faces
% of the possibly PML augmented domain.  The source term S consists of
% several point sources; otherwise line, face or volume source requires
% numerical integration, which we do not consider for the moment.  To avoid
% numerical integration, we always assume homogeneous boundary conditions.
%
% The tangential operator T is described by
%
%       T = p1 - p2 D_{tt}
%
% where p1, p2 complex-valued and constant on each element.
%
% The PML outside a face is described by
%
%       eta: number of elements of the artificial layer outside the faces on
%       left, right, front, back, bottom and top faces
%
%       sigma: complex-valued function in the layer [0,L], zero on the
%       physical face L, e.g.
%
%               sigma(t,L) = C/L *((t-L)/L)^m,
%
%       where C = 3 (m+1) pi or some other appropriate constants.
%
% Then, the PDE in PML is obtained by replacing D with s*D,
%
%   rho xi1*Dx(1/rho xi1*Dx u) + rho xi2*Dy(1/rho xi2*Dy u) +
%         rho xi3*Dz(1/rho xi3*Dz u) + w^2*u = -S,
%
% where xi1(x) = 1/(1+1i*sigma1(x)/w), xi2(y) = 1/(1+1i*sigma2(y)/w), xi3 is
% similar, w is the wavenumber.
%
%
%
% We use a uniform cartesian mesh and discretize it by cosine expansion
% of Neumann traces on element faces.

clear all;

%   define the continuous problem
global helmprb;
helmprb.domain= [0 1; 0 1; 0 1];        % xl, xr, yl, yr, zl, zr
helmprb.f= 0.9;                           % frequency
helmprb.file4c= [];                     % empty to use default value one
helmprb.file4a= [];                     % empty to use default value one
helmprb.bctype= [1 1 1 1 1 1];        % left, right, front, back, bottome, top
helmprb.sgn= 1;                         % sign convention
helmprb.p= @(w) helmprb.sgn*1i*[w -0.5/w 1.5*w];% Robin parameters
helmprb.eta= [0 0 0 0 0 0];           % parameters for PML
helmprb.sigma= @(t,L) 9*pi/L*((t-L)/L)^2;
helmprb.scpoints= [.5 .5 .5];%[.53 .53 .9; .53 .23 .9; .53 .76 .9];% location of point sources, (x,y,z)
helmprb.Sp= ones(size(helmprb.scpoints,1),1);% strength of point sources

%   mesh parameters
helmprb.n= ones(3,1);%helmprb.f*[10; 10; 10];      % number of elements in the original
                                        % domain (excluding PML)
helmprb.h= diff(helmprb.domain,1,2)./helmprb.n;

%   arrays of density and wavenumber, first z, y, then x
if isempty(helmprb.file4a)
    helmprb.rho= ones(helmprb.n(3),helmprb.n(2),helmprb.n(1));
else
    % read in rho from file4a
    error('variable density is not supported yet');
end
if isempty(helmprb.file4c)
    helmprb.w= 2*pi*helmprb.f*ones(helmprb.n(3),helmprb.n(2),helmprb.n(1));
else
    % read in c from file4c
    error('variable velocity is not supported yet');
end

%   more parameters for discretization
helmprb.tgv= 1;                         % small or large number for Neumann
helmprb.npml= augpml(helmprb.n,helmprb.eta);
initnf();
[m1f,m2f]= ndgrid(0:2,0:2);
initdoff(m1f,m2f);

%   linear system
A= assembly(helmprb);
b= rhs(helmprb);

%   direct solver
un= A\b;

%   visualization
x= linspace(helmprb.domain(1,1),helmprb.domain(1,2),2*helmprb.n(1)+1);
y= linspace(helmprb.domain(2,1),helmprb.domain(2,2),2*helmprb.n(2)+1);
z= linspace(helmprb.domain(3,1),helmprb.domain(3,2),2*helmprb.n(3)+1);
[x y z]= meshgrid(x,y,z);
u= evalsol(helmprb,un,x,y,z);
figure, set(gcf,'renderer','painters'); slice(x,y,z,real(u),.5,.5,.5), colorbar;
% $$$ figure, plot(y(:,26,46),u(:,26,46));
figure, plot(y(:,41,73),u(:,41,73),'c');

%   exact solution for constant rho and w
% $$$ uexact= GreenFun(helmprb.scpoints,x,y,z,helmprb.w(1),helmprb.sgn);
% $$$ figure, slice(x,y,z,real(uexact),.5,.5,.5),colorbar;