function b = rhs(helmprb)
%rhs.m -- Compute the right hand side for the 'helmprb'.
%
%helmprb.scpoints are physical coordinates in the original domain.  No
%complex coordinates streching, so xi= 1.

%   initialization
h= helmprb.h;
b= zeros(helmprb.dofend(helmprb.tnf)-1,1);

%   loop over every point source
for isrc= 1:size(helmprb.scpoints,1)
    %   strength of this point source
    Sp= helmprb.Sp(isrc);

    %   3D subscripts of the element, base index 1
    subphy= 1+floor((helmprb.scpoints(isrc,:).'-helmprb.domain(:,1))./h);
    subphy= min(subphy,helmprb.n);
    subpml= subphy + helmprb.eta([1 3 5]).';

    %   wavenumber of this element
    wele= helmprb.w(subphy(1),subphy(2),subphy(3));

    %   reference coordinates of this point source in the reference element [0,h]
    r0= helmprb.scpoints(isrc,:)-helmprb.domain(:,1).'-((subphy-1).*h).';

    %-{ rows corresponding to tests with left face modes, sign with +
    %   opppsite to the sign for the matrix
    idx= sub2idx(1,subpml,helmprb.npml,helmprb.nf);
    for idof= helmprb.dofbegin(idx):helmprb.dofend(idx)-1
        ky= helmprb.m1(idof)*pi/h(2);
        kz= helmprb.m2(idof)*pi/h(3);
        s= sqrt(ky^2+kz^2-wele^2);
        b(idof)= b(idof)+Sp/s*(exp(-s*(r0(1)-h(1)))+exp(s*(r0(1)-h(1))))/(-exp(-s*h(1))+exp(s*h(1)))*cos(ky*r0(2))*cos(kz*r0(3));
    end
    %-}

    %-{ rows corresponding to tests with right face modes, sign with -
    idx= sub2idx(1,subpml+[1;0;0],helmprb.npml,helmprb.nf);
    for idof= helmprb.dofbegin(idx):helmprb.dofend(idx)-1
        ky= helmprb.m1(idof)*pi/h(2);
        kz= helmprb.m2(idof)*pi/h(3);
        s= sqrt(ky^2+kz^2-wele^2);
        b(idof)= b(idof)-Sp/s*(exp(-s*r0(1))+exp(s*r0(1)))/(-exp(-s*h(1))+exp(s*h(1)))*cos(ky*r0(2))*cos(kz*r0(3));
    end
    %-}

    %-{ rows corresponding to tests with front face modes, sign with +
    idx= sub2idx(2,subpml,helmprb.npml,helmprb.nf);
    for idof= helmprb.dofbegin(idx):helmprb.dofend(idx)-1
        kx= helmprb.m1(idof)*pi/h(1);
        kz= helmprb.m2(idof)*pi/h(3);
        s= sqrt(kx^2+kz^2-wele^2);
        b(idof)= b(idof)+Sp/s*(exp(-s*(r0(2)-h(2)))+exp(s*(r0(2)-h(2))))/(-exp(-s*h(2))+exp(s*h(2)))*cos(kx*r0(1))*cos(kz*r0(3));
    end
    %-}

    %-{ rows corresponding to tests with back face modes, sign with -
    idx= sub2idx(2,subpml+[0;1;0],helmprb.npml,helmprb.nf);
    for idof= helmprb.dofbegin(idx):helmprb.dofend(idx)-1
        kx= helmprb.m1(idof)*pi/h(1);
        kz= helmprb.m2(idof)*pi/h(3);
        s= sqrt(kx^2+kz^2-wele^2);
        b(idof)= b(idof)-Sp/s*(exp(-s*r0(2))+exp(s*r0(2)))/(-exp(-s*h(2))+exp(s*h(2)))*cos(kx*r0(1))*cos(kz*r0(3));
    end
    %-}

    %-{ rows corresponding to tests with bottom face modes, sign with +
    idx= sub2idx(3,subpml,helmprb.npml,helmprb.nf);
    for idof= helmprb.dofbegin(idx):helmprb.dofend(idx)-1
        kx= helmprb.m1(idof)*pi/h(1);
        ky= helmprb.m2(idof)*pi/h(2);
        s= sqrt(kx^2+ky^2-wele^2);
        b(idof)= b(idof)+Sp/s*(exp(-s*(r0(3)-h(3)))+exp(s*(r0(3)-h(3))))/(-exp(-s*h(3))+exp(s*h(3)))*cos(kx*r0(1))*cos(ky*r0(2));
    end
    %-}

    %-{ rows corresponding to tests with top face modes, sign with -
    idx= sub2idx(3,subpml+[0;0;1],helmprb.npml,helmprb.nf);
    for idof= helmprb.dofbegin(idx):helmprb.dofend(idx)-1
        kx= helmprb.m1(idof)*pi/h(1);
        ky= helmprb.m2(idof)*pi/h(2);
        s= sqrt(kx^2+ky^2-wele^2);
        b(idof)= b(idof)-Sp/s*(exp(-s*r0(3))+exp(s*r0(3)))/(-exp(-s*h(3))+exp(s*h(3)))*cos(kx*r0(1))*cos(ky*r0(2));
    end
    %-}

end

%   treat Neumann b.c., assuming homogeneous conditions

%-{ left face of the domain
if helmprb.bctype(1)==0
    for iz= 1:helmprb.npml(3)
        for iy= 1:helmprb.npml(2)
            idx= sub2idx(1,[1 iy iz],helmprb.npml,helmprb.nf);
            for idof= helmprb.dofbegin(idx):helmprb.dofend(idx)-1
                b(idof)= 0;
            end
        end
    end
end
%-}

%-{ right face of the domain
if helmprb.bctype(2)==0
    for iz= 1:helmprb.npml(3)
        for iy= 1:helmprb.npml(2)
            idx= sub2idx(1,[helmprb.npml(1)+1 iy iz],helmprb.npml,helmprb.nf);
            for idof= helmprb.dofbegin(idx):helmprb.dofend(idx)-1
                b(idof)= 0;
            end
        end
    end
end
%-}

%-{ front face of the domain
if helmprb.bctype(3)==0
    for iz= 1:helmprb.npml(3)
        for ix= 1:helmprb.npml(1)
            idx= sub2idx(2,[ix 1 iz],helmprb.npml,helmprb.nf);
            for idof= helmprb.dofbegin(idx):helmprb.dofend(idx)-1
                b(idof)= 0;
            end
        end
    end
end
%-}

%-{ back face of the domain
if helmprb.bctype(4)==0
    for iz= 1:helmprb.npml(3)
        for ix= 1:helmprb.npml(1)
            idx= sub2idx(2,[ix helmprb.npml(2)+1 iz],helmprb.npml,helmprb.nf);
            for idof= helmprb.dofbegin(idx):helmprb.dofend(idx)-1
                b(idof)= 0;
            end
        end
    end
end
%-}

%-{ bottom face of the domain
if helmprb.bctype(5)==0
    for ix= 1:helmprb.npml(1)
        for iy= 1:helmprb.npml(2)
            idx= sub2idx(3,[ix iy 1],helmprb.npml,helmprb.nf);
            for idof= helmprb.dofbegin(idx):helmprb.dofend(idx)-1
                b(idof)= 0;
            end
        end
    end
end
%-}

%-{ top face of the domain
if helmprb.bctype(6)==0
    for ix= 1:helmprb.npml(1)
        for iy= 1:helmprb.npml(2)
            idx= sub2idx(3,[ix iy helmprb.npml(3)+1],helmprb.npml,helmprb.nf);
            for idof= helmprb.dofbegin(idx):helmprb.dofend(idx)-1
                b(idof)= 0;
            end
        end
    end
end
%-}


%   treat Robin b.c., assuming homogeneous conditions

%-{ left face of the domain
if helmprb.bctype(1)==1
    for iy= 1:helmprb.npml(2)
        for iz= 1:helmprb.npml(3)
            idx= sub2idx(1,[1 iy iz],helmprb.npml,helmprb.nf);
            subphy= subpml2phy([1,iy,iz]);
            pele= helmprb.p(helmprb.w(subphy(1),subphy(2),subphy(3)));
            for idof= helmprb.dofbegin(idx):helmprb.dofend(idx)-1
                ky= helmprb.m1(idof)*pi/h(2);
                kz= helmprb.m2(idof)*pi/h(3);
                b(idof)= b(idof)*(pele(1)+pele(2)*(ky^2+kz^2));
            end
        end
    end
end
%-}

%-{ right face of the domain
if helmprb.bctype(2)==1
    for iy= 1:helmprb.npml(2)
        for iz= 1:helmprb.npml(3)
            idx= sub2idx(1,[helmprb.npml(1)+1 iy iz],helmprb.npml,helmprb.nf);
            subphy= subpml2phy([helmprb.npml(1),iy,iz]);
            pele= helmprb.p(helmprb.w(subphy(1),subphy(2),subphy(3)));
            for idof= helmprb.dofbegin(idx):helmprb.dofend(idx)-1
                ky= helmprb.m1(idof)*pi/h(2);
                kz= helmprb.m2(idof)*pi/h(3);
                b(idof)= b(idof)*(pele(1)+pele(2)*(ky^2+kz^2));
            end
        end
    end
end
%-}

%-{ front face of the domain
if helmprb.bctype(3)==1
    for ix= 1:helmprb.npml(1)
        for iz= 1:helmprb.npml(3)
            idx= sub2idx(2,[ix 1 iz],helmprb.npml,helmprb.nf);
            subphy= subpml2phy([ix,1,iz]);
            pele= helmprb.p(helmprb.w(subphy(1),subphy(2),subphy(3)));
            for idof= helmprb.dofbegin(idx):helmprb.dofend(idx)-1
                kx= helmprb.m1(idof)*pi/h(1);
                kz= helmprb.m2(idof)*pi/h(3);
                b(idof)= b(idof)*(pele(1)+pele(2)*(kx^2+kz^2));
            end
        end
    end
end
%-}

%-{ back face of the domain
if helmprb.bctype(4)==1
    for ix= 1:helmprb.npml(1)
        for iz= 1:helmprb.npml(3)
            idx= sub2idx(2,[ix helmprb.npml(2)+1 iz],helmprb.npml,helmprb.nf);
            subphy= subpml2phy([ix,helmprb.npml(2),iz]);
            pele= helmprb.p(helmprb.w(subphy(1),subphy(2),subphy(3)));
            for idof= helmprb.dofbegin(idx):helmprb.dofend(idx)-1
                kx= helmprb.m1(idof)*pi/h(1);
                kz= helmprb.m2(idof)*pi/h(3);
                b(idof)= b(idof)*(pele(1)+pele(2)*(kx^2+kz^2));
            end
        end
    end
end
%-}

%-{ bottom face of the domain
if helmprb.bctype(5)==1
    for ix= 1:helmprb.npml(1)
        for iy= 1:helmprb.npml(2)
            idx= sub2idx(3,[ix iy 1],helmprb.npml,helmprb.nf);
            subphy= subpml2phy([ix,iy,1]);
            pele= helmprb.p(helmprb.w(subphy(1),subphy(2),subphy(3)));
            for idof= helmprb.dofbegin(idx):helmprb.dofend(idx)-1
                kx= helmprb.m1(idof)*pi/h(1);
                ky= helmprb.m2(idof)*pi/h(2);
                b(idof)= b(idof)*(pele(1)+pele(2)*(kx^2+ky^2));
            end
        end
    end
end
%-}

%-{ top face of the domain
if helmprb.bctype(6)==1
    for ix= 1:helmprb.npml(1)
        for iy= 1:helmprb.npml(2)
            idx= sub2idx(3,[ix iy helmprb.npml(3)+1],helmprb.npml,helmprb.nf);
            subphy= subpml2phy([ix,iy,helmprb.npml(3)]);
            pele= helmprb.p(helmprb.w(subphy(1),subphy(2),subphy(3)));
            for idof= helmprb.dofbegin(idx):helmprb.dofend(idx)-1
                kx= helmprb.m1(idof)*pi/h(1);
                ky= helmprb.m2(idof)*pi/h(2);
                b(idof)= b(idof)*(pele(1)+pele(2)*(kx^2+ky^2));
            end
        end
    end
end
%-}