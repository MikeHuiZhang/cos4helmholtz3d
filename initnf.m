function initnf()

global helmprb;

%   number of elements in pml augmented domain 
n= helmprb.npml;

%   number of faces
helmprb.nf= zeros(3,1);
helmprb.nf(1)= n(3)*n(2)*(n(1)+1);
helmprb.nf(2)= n(3)*n(1)*(n(2)+1);
helmprb.nf(3)= n(2)*n(1)*(n(3)+1);
helmprb.tnf= sum(helmprb.nf);
