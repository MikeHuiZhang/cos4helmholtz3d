function re = subpml2phy(subpml)
% Given 3D subscripts of an element in the pml augmented mesh, return the 3D
% subscripts of the element (which can be different from the given element) in
% the original mesh for reading rho and w.
%

global helmprb;
eta= helmprb.eta;
n= helmprb.n;

% base index 1, up onto end
    ix= max(subpml(1),1+eta(1));
    ix= min(ix,n(1)+eta(1));
    iy= max(subpml(2),1+eta(3));
    iy= min(iy,n(2)+eta(3));
    iz= max(subpml(3),1+eta(5));
    iz= min(iz,n(3)+eta(5));
    re= [ix-eta(1) iy-eta(3) iz-eta(5)];
end