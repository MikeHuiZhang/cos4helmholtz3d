function idx = sub2idx(dire,sub,n,nf)
%sub2idx -- Given 3D subscripts of a face and its normal direction, compute
%its 1D index in all faces.

%  first comes all faces with normal direction x, then y,z
%  base index 1, first varies in z, then y, x
if dire==1                              % normal x
    idx= (sub(1)-1)*n(3)*n(2)+(sub(2)-1)*n(3)+sub(3);
elseif dire==2
    idx= nf(1)+(sub(1)-1)*n(3)*(n(2)+1)+(sub(2)-1)*n(3)+sub(3);
elseif dire==3
    idx= nf(1)+nf(2)+(sub(1)-1)*(n(3)+1)*n(2)+(sub(2)-1)*(n(3)+1)+sub(3);
end