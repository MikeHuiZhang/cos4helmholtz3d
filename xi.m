function re = xi(subpml,subphy)
% For the element with 3D subscripts 'subpml' in the pml augmented mesh, compute
% [xi_1,xi_2,xi_3] for complex-streching of pml, by which D -> xi.*D.
% 

global helmprb;
w= helmprb.w(subphy(1),subphy(2),subphy(3));
sigma= helmprb.sigma;
eta= helmprb.eta;
npml= helmprb.npml;

% base index 1, up onto end
re= ones(1,3);
for dim= 1:3
    h= helmprb.h(dim);
    if subpml(dim)<helmprb.eta(dim)+1
        % take value of sigma at the left boundary of the element
        re(dim)= 1/(1+1i/w*sigma((subpml(dim)-1)*h,eta(2*dim-1)*h));
    elseif subpml(dim)>helmprb.n(dim)+helmprb.eta(dim)
        % take value of sigma at the right boundary of the element
        re(dim)= 1/(1+1i/w*sigma((npml(dim)-subpml(dim))*h, eta(2*dim)*h));
    else
        re(dim)= 1;
    end    
end