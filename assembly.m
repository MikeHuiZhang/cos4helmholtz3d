function A = assembly(helmprb)
%assembly -- compute the matrix for the simple discretization of Helmholtz
%equation in 3D with possible PML

%   initialization
%
% Estimate number of non-zeros in A.  Each face interacts with itself and the
% other 10 neighboring faces in 3D, parallel face interaction are diagonal,
% non-parallel face interaction are non-zero only for their common edge modes.
maxdoff= max(helmprb.ndoff);
maxnnz= (3 + 4*maxdoff)*(helmprb.dofend(end)-1);
ii= zeros(maxnnz,1); jj= zeros(maxnnz,1); vv= zeros(maxnnz,1);
h= helmprb.h;


%   loop over every element, Dirichlet b.c. is obtained 
count= 0;
for iele= 1:prod(helmprb.npml)
    [ix,iy,iz]= ind2sub(helmprb.npml.',iele);
    subphy= subpml2phy([ix,iy,iz]);
    xiele= xi([ix,iy,iz],subphy);
    rhoele= helmprb.rho(subphy(3),subphy(2),subphy(1));
    wele= helmprb.w(subphy(3),subphy(2),subphy(1));
    %   idx of all faces in this elemnt: left,right,front,back,bottom,top
    idx(1)= sub2idx(1,[ix iy iz],helmprb.npml,helmprb.nf);
    idx(2)= sub2idx(1,[ix+1 iy iz],helmprb.npml,helmprb.nf);
    idx(3)= sub2idx(2,[ix iy iz],helmprb.npml,helmprb.nf);
    idx(4)= sub2idx(2,[ix iy+1 iz],helmprb.npml,helmprb.nf);
    idx(5)= sub2idx(3,[ix iy iz],helmprb.npml,helmprb.nf);
    idx(6)= sub2idx(3,[ix iy iz+1],helmprb.npml,helmprb.nf);
    %-{   rows of left face: *test* by left face modes, sign Dirichlet with -
    for idof= helmprb.dofbegin(idx(1)):helmprb.dofend(idx(1))-1
        my= helmprb.m1(idof);
        mz= helmprb.m2(idof);
        ky= my*pi/h(2);
        kz= mz*pi/h(3);
        Cyz= 0.25*(1+(ky==0))*(1+(kz==0));
        Cy= 0.5*(1+(ky==0));
        Cz= 0.5*(1+(kz==0));

        %-{ self interaction
        count= count+1;
        ii(count)= idof;
        jj(count)= idof;
        s= sqrt((xiele(2)*ky)^2+(xiele(3)*kz)^2-wele^2)/xiele(1);
        vv(count)= Cyz*h(2)*h(3)*rhoele/xiele(1)/s*(exp((-s*h(1)))+exp(s*h(1)))/(-exp((-s*h(1)))+exp(s*h(1)));
        %-}

        %-{ parallel interaction
        for jdof= helmprb.dofbegin(idx(2)):helmprb.dofend(idx(2))-1
            if helmprb.m1(jdof)==helmprb.m1(idof) && helmprb.m2(jdof)==helmprb.m2(idof)
                count= count+1;
                ii(count)= idof;
                jj(count)= jdof;
                %s= sfun(1,ky,kz,xiele,wele);
                vv(count)= -2.0*Cyz*h(2)*h(3)*rhoele/xiele(1)/s/(-exp((-s*h(1)))+exp(s*h(1)));
                break;
            end
        end
        %-}

        %-{ non-parallel interaction with front face
        for jdof= helmprb.dofbegin(idx(3)):helmprb.dofend(idx(3))-1
            if helmprb.m2(jdof)==helmprb.m2(idof)
                count= count+1;
                ii(count)= idof;
                jj(count)= jdof;
                kx= helmprb.m1(jdof)*pi/h(1);
                s= sqrt((xiele(1)*kx)^2+(xiele(3)*kz)^2-wele^2)/xiele(2);
                %sfun(2,kx,kz,xiele,wele);
                vv(count)= Cz*h(3)*rhoele/xiele(2)/(s^2+ky^2);
            end
        end
        %-}

        %-{ non-parallel interaction with back face
        for jdof= helmprb.dofbegin(idx(4)):helmprb.dofend(idx(4))-1
            if helmprb.m2(jdof)==helmprb.m2(idof)
                count= count+1;
                ii(count)= idof;
                jj(count)= jdof;
                kx= helmprb.m1(jdof)*pi/h(1);
                s= sqrt((xiele(1)*kx)^2+(xiele(3)*kz)^2-wele^2)/xiele(2);
                %s= sfun(2,kx,kz,xiele,wele);
                vv(count)= -Cz*h(3)*rhoele*cos(my*pi)/xiele(2)/(s^2+ky^2);
            end
        end
        %-}

        %-{ non-parallel interaction with bottom face
        for jdof= helmprb.dofbegin(idx(5)):helmprb.dofend(idx(5))-1
            if helmprb.m2(jdof)==helmprb.m1(idof)
                count= count+1;
                ii(count)= idof;
                jj(count)= jdof;
                kx= helmprb.m1(jdof)*pi/h(1);
                s= sqrt((xiele(1)*kx)^2+(xiele(2)*ky)^2-wele^2)/xiele(3);
                %s= sfun(3,kx,ky,xiele,wele);
                vv(count)= Cy*h(2)*rhoele/xiele(3)/(s^2+kz^2);
            end
        end
        %-}

        %-{ non-parallel interaction with top face
        for jdof= helmprb.dofbegin(idx(6)):helmprb.dofend(idx(6))-1
            if helmprb.m2(jdof)==helmprb.m1(idof)
                count= count+1;
                ii(count)= idof;
                jj(count)= jdof;
                kx= helmprb.m1(jdof)*pi/h(1);
                s= sqrt((xiele(1)*kx)^2+(xiele(2)*ky)^2-wele^2)/xiele(3);
                %s= sfun(3,kx,ky,xiele,wele);
                vv(count)= -Cy*h(2)*rhoele*cos(mz*pi)/xiele(3)/(s^2+kz^2);
            end
        end
        %-}

    end
    %-}

    %-{   rows of right face: *test* by right face modes, Dirichlet sign +
    for idof= helmprb.dofbegin(idx(2)):helmprb.dofend(idx(2))-1
        my= helmprb.m1(idof);
        mz= helmprb.m2(idof);
        ky= my*pi/h(2);
        kz= mz*pi/h(3);
        Cyz= 0.25*(1+(ky==0))*(1+(kz==0));
        Cy= 0.5*(1+(ky==0));
        Cz= 0.5*(1+(kz==0));

        %-{ self interaction
        count= count+1;
        ii(count)=  idof;
        jj(count)= ii(count);
        s= sqrt((xiele(2)*ky)^2+(xiele(3)*kz)^2-wele^2)/xiele(1);
        %s= sfun(1,ky,kz,xiele,wele);
        vv(count)= Cyz*h(2)*h(3)*rhoele/xiele(1)/ ...
            s*(exp((-s*h(1)))+exp(s*h(1)))/(-exp((-s*h(1)))+exp(s*h(1)));
        %-}

        %-{ parallel interaction
        for jdof= helmprb.dofbegin(idx(1)):helmprb.dofend(idx(1))-1
            if helmprb.m1(jdof)==helmprb.m1(idof) && helmprb.m2(jdof)==helmprb.m2(idof)
                count= count+1;
                ii(count)= idof;
                jj(count)= jdof;
                %s= sfun(1,ky,kz,xiele,wele);
                vv(count)= -2.0*Cyz*h(2)*h(3)*rhoele/xiele(1)/s/(-exp((-s*h(1)))+exp(s*h(1)));
                break;
            end
        end
        %-}


        %-{ non-parallel interaction with front face
        for jdof= helmprb.dofbegin(idx(3)):helmprb.dofend(idx(3))-1
            if helmprb.m2(jdof)==helmprb.m2(idof)
                count= count+1;
                ii(count)= idof;
                jj(count)= jdof;
                mx= helmprb.m1(jdof);
                kx= mx*pi/h(1);
                s= sqrt((xiele(1)*kx)^2+(xiele(3)*kz)^2-wele^2)/xiele(2);
                %s= sfun(2,kx,kz,xiele,wele);
                vv(count)= -Cz*h(3)*rhoele*cos(mx*pi)/xiele(2)/(s^2+ky^2);
            end
        end
        %-}

        %-{ non-parallel interaction with back face
        for jdof= helmprb.dofbegin(idx(4)):helmprb.dofend(idx(4))-1
            if helmprb.m2(jdof)==helmprb.m2(idof)
                count= count+1;
                ii(count)= idof;
                jj(count)= jdof;
                mx= helmprb.m1(jdof);
                kx= mx*pi/h(1);
                s= sqrt((xiele(1)*kx)^2+(xiele(3)*kz)^2-wele^2)/xiele(2);
                %s= sfun(2,kx,kz,xiele,wele);
                vv(count)= Cz*h(3)*rhoele*cos(my*pi)*cos(mx*pi)/xiele(2)/(s^2+ky^2);
            end
        end
        %-}

        %-{ non-parallel interaction with bottom face
        for jdof= helmprb.dofbegin(idx(5)):helmprb.dofend(idx(5))-1
            if helmprb.m2(jdof)==helmprb.m1(idof)
                count= count+1;
                ii(count)= idof;
                jj(count)= jdof;
                mx= helmprb.m1(jdof);
                kx= mx*pi/h(1);
                s= sqrt((xiele(1)*kx)^2+(xiele(2)*ky)^2-wele^2)/xiele(3);
                %s= sfun(3,kx,ky,xiele,wele);
                vv(count)= -Cy*h(2)*rhoele*cos(mx*pi)/xiele(3)/(s^2+kz^2);
            end
        end
        %-}

        %-{ non-parallel interaction with top face
        for jdof= helmprb.dofbegin(idx(6)):helmprb.dofend(idx(6))-1
            if helmprb.m2(jdof)==helmprb.m1(idof)
                count= count+1;
                ii(count)= idof;
                jj(count)= jdof;
                mx= helmprb.m1(jdof);
                kx= mx*pi/h(1);
                s= sqrt((xiele(1)*kx)^2+(xiele(2)*ky)^2-wele^2)/xiele(3);
                %s= sfun(3,kx,ky,xiele,wele);
                vv(count)= Cy*h(2)*rhoele*cos(mz*pi)*cos(mx*pi)/xiele(3)/(s^2+kz^2);
            end
        end
        %-}

    end
    %-}

    %-{   rows of front face: *test* by front face modes, sign Dirichlet with -
    for idof= helmprb.dofbegin(idx(3)):helmprb.dofend(idx(3))-1
        mx= helmprb.m1(idof);
        mz= helmprb.m2(idof);
        kx= mx*pi/h(1);
        kz= mz*pi/h(3);
        Cxz= 0.25*(1+(kx==0))*(1+(kz==0));
        Cx= 0.5*(1+(kx==0));
        Cz= 0.5*(1+(kz==0));

        %-{ self interaction
        count= count+1;
        ii(count)=  idof;
        jj(count)= ii(count);
        s= sqrt((xiele(1)*kx)^2+(xiele(3)*kz)^2-wele^2)/xiele(2);
        %s= sfun(2,kx,kz,xiele,wele);
        vv(count)= Cxz*h(1)*h(3)*rhoele/xiele(2)/ ...
            s*(exp((-s*h(2)))+exp(s*h(2)))/(-exp((-s*h(2)))+exp(s*h(2)));
        %-}

        %-{ parallel interaction
        for jdof= helmprb.dofbegin(idx(4)):helmprb.dofend(idx(4))-1
            if helmprb.m1(jdof)==helmprb.m1(idof) && helmprb.m2(jdof)==helmprb.m2(idof)
                count= count+1;
                ii(count)= idof;
                jj(count)= jdof;
                %s= sfun(2,kx,kz,xiele,wele);
                vv(count)= -2.0*Cxz*h(1)*h(3)*rhoele/xiele(2)/s/(-exp((-s*h(2)))+exp(s*h(2)));
                break;
            end
        end
        %-}


        %-{ non-parallel interaction with left face
        for jdof= helmprb.dofbegin(idx(1)):helmprb.dofend(idx(1))-1
            if helmprb.m2(jdof)==helmprb.m2(idof)
                count= count+1;
                ii(count)= idof;
                jj(count)= jdof;
                ky= helmprb.m1(jdof)*pi/h(2);
                s= sqrt((xiele(2)*ky)^2+(xiele(3)*kz)^2-wele^2)/xiele(1);
                %s= sfun(1,ky,kz,xiele,wele);
                vv(count)= Cz*h(3)*rhoele/xiele(1)/(s^2+kx^2);
            end
        end
        %-}

        %-{ non-parallel interaction with right face
        for jdof= helmprb.dofbegin(idx(2)):helmprb.dofend(idx(2))-1
            if helmprb.m2(jdof)==helmprb.m2(idof)
                count= count+1;
                ii(count)= idof;
                jj(count)= jdof;
                ky= helmprb.m1(jdof)*pi/h(2);
                s= sqrt((xiele(2)*ky)^2+(xiele(3)*kz)^2-wele^2)/xiele(1);
                %s= sfun(1,ky,kz,xiele,wele);
                vv(count)= -Cz*h(3)*rhoele*cos(mx*pi)/xiele(1)/(s^2+kx^2);
            end
        end
        %-}

        %-{ non-parallel interaction with bottom face
        for jdof= helmprb.dofbegin(idx(5)):helmprb.dofend(idx(5))-1
            if helmprb.m1(jdof)==helmprb.m1(idof)
                count= count+1;
                ii(count)= idof;
                jj(count)= jdof;
                ky= helmprb.m2(jdof)*pi/h(2);
                s= sqrt((xiele(1)*kx)^2+(xiele(2)*ky)^2-wele^2)/xiele(3);
                %s= sfun(3,kx,ky,xiele,wele);
                vv(count)= Cx*h(1)*rhoele/xiele(3)/(s^2+kz^2);
            end
        end
        %-}

        %-{ non-parallel interaction with top face
        for jdof= helmprb.dofbegin(idx(6)):helmprb.dofend(idx(6))-1
            if helmprb.m1(jdof)==helmprb.m1(idof)
                count= count+1;
                ii(count)= idof;
                jj(count)= jdof;
                ky= helmprb.m2(jdof)*pi/h(2);
                s= sqrt((xiele(1)*kx)^2+(xiele(2)*ky)^2-wele^2)/xiele(3);
                %s= sfun(3,kx,ky,xiele,wele);
                vv(count)= -Cx*h(1)*rhoele*cos(mz*pi)/xiele(3)/(s^2+kz^2);
            end
        end
        %-}
    end
    %-}

    %-{   rows of back face: *test* by back face modes, sign Dirichlet with +
    for idof= helmprb.dofbegin(idx(4)):helmprb.dofend(idx(4))-1
        mx= helmprb.m1(idof);
        mz= helmprb.m2(idof);
        kx= mx*pi/h(1);
        kz= mz*pi/h(3);
        Cxz= 0.25*(1+(kx==0))*(1+(kz==0));
        Cx= 0.5*(1+(kx==0));
        Cz= 0.5*(1+(kz==0));


        %-{ self interaction
        count= count+1;
        ii(count)=  idof;
        jj(count)= ii(count);
        s= sqrt((xiele(1)*kx)^2+(xiele(3)*kz)^2-wele^2)/xiele(2);
        %s= sfun(2,kx,kz,xiele,wele);
        vv(count)= Cxz*h(1)*h(3)*rhoele/xiele(2)/ ...
            s*(exp((-s*h(2)))+exp(s*h(2)))/(-exp((-s*h(2)))+exp(s*h(2)));
        %-}

        %-{ parallel interaction
        for jdof= helmprb.dofbegin(idx(3)):helmprb.dofend(idx(3))-1
            if helmprb.m1(jdof)==helmprb.m1(idof) && helmprb.m2(jdof)==helmprb.m2(idof)
                count= count+1;
                ii(count)= idof;
                jj(count)= jdof;
                %s= sfun(2,kx,kz,xiele,wele);
                vv(count)= -2.0*Cxz*h(1)*h(3)*rhoele/xiele(2)/s/(-exp((-s*h(2)))+exp(s*h(2)));
                break;
            end
        end
        %-}

        %-{ non-parallel interaction with left face
        for jdof= helmprb.dofbegin(idx(1)):helmprb.dofend(idx(1))-1
            if helmprb.m2(jdof)==helmprb.m2(idof)
                count= count+1;
                ii(count)= idof;
                jj(count)= jdof;
                my= helmprb.m1(jdof);
                ky= my*pi/h(2);
                s= sqrt((xiele(2)*ky)^2+(xiele(3)*kz)^2-wele^2)/xiele(1);
                %s= sfun(1,ky,kz,xiele,wele);
                vv(count)= -Cz*h(3)*rhoele*cos(my*pi)/xiele(1)/(s^2+kx^2);
            end
        end
        %-}

        %-{ non-parallel interaction with right face
        for jdof= helmprb.dofbegin(idx(2)):helmprb.dofend(idx(2))-1
            if helmprb.m2(jdof)==helmprb.m2(idof)
                count= count+1;
                ii(count)= idof;
                jj(count)= jdof;
                my= helmprb.m1(jdof);
                ky= my*pi/h(2);
                s= sqrt((xiele(2)*ky)^2+(xiele(3)*kz)^2-wele^2)/xiele(1);
                %s= sfun(1,ky,kz,xiele,wele);
                vv(count)= Cz*h(3)*rhoele*cos(mx*pi)*cos(my*pi)/xiele(1)/(s^2+kx^2);
            end
        end
        %-}

        %-{ non-parallel interaction with bottom face
        for jdof= helmprb.dofbegin(idx(5)):helmprb.dofend(idx(5))-1
            if helmprb.m1(jdof)==helmprb.m1(idof)
                count= count+1;
                ii(count)= idof;
                jj(count)= jdof;
                my= helmprb.m2(jdof);
                ky= my*pi/h(2);
                s= sqrt((xiele(1)*kx)^2+(xiele(2)*ky)^2-wele^2)/xiele(3);
                %s= sfun(3,kx,ky,xiele,wele);
                vv(count)= -Cx*h(1)*rhoele*cos(my*pi)/xiele(3)/(s^2+kz^2);
            end
        end
        %-}

        %-{ non-parallel interaction with top face
        for jdof= helmprb.dofbegin(idx(6)):helmprb.dofend(idx(6))-1
            if helmprb.m1(jdof)==helmprb.m1(idof)
                count= count+1;
                ii(count)= idof;
                jj(count)= jdof;
                my= helmprb.m2(jdof);
                ky= my*pi/h(2);
                s= sqrt((xiele(1)*kx)^2+(xiele(2)*ky)^2-wele^2)/xiele(3);
                %s= sfun(3,kx,ky,xiele,wele);
                vv(count)= Cx*h(1)*rhoele*cos(mz*pi)*cos(my*pi)/xiele(3)/(s^2+kz^2);
            end
        end
        %-}
    end
    %-}

    %-{   rows of bottom face: *test* by bottom face modes, sign Dirichlet with -
    for idof= helmprb.dofbegin(idx(5)):helmprb.dofend(idx(5))-1
        mx= helmprb.m1(idof);
        my= helmprb.m2(idof);
        kx= mx*pi/h(1);
        ky= my*pi/h(2);
        Cxy= 0.25*(1+(ky==0))*(1+(kx==0));
        Cy= 0.5*(1+(ky==0));
        Cx= 0.5*(1+(kx==0));


        %-{ self interaction
        count= count+1;
        ii(count)=  idof;
        jj(count)= ii(count);
        s= sqrt((xiele(1)*kx)^2+(xiele(2)*ky)^2-wele^2)/xiele(3);
        %s= sfun(3,kx,ky,xiele,wele);
        vv(count)= Cxy*h(1)*h(2)*rhoele/xiele(3)/ ...
            s*(exp((-s*h(3)))+exp(s*h(3)))/(-exp((-s*h(3)))+exp(s*h(3)));
        %-}
        %-{ parallel interaction
        for jdof= helmprb.dofbegin(idx(6)):helmprb.dofend(idx(6))-1
            if helmprb.m1(jdof)==helmprb.m1(idof) && helmprb.m2(jdof)==helmprb.m2(idof)
                count= count+1;
                ii(count)= idof;
                jj(count)= jdof;
                %s= sfun(3,kx,ky,xiele,wele);
                vv(count)= -2.0*Cxy*h(1)*h(2)*rhoele/xiele(3)/s/(-exp((-s*h(3)))+exp(s*h(3)));
                break;
            end
        end
        %-}

        %-{ non-parallel interaction with front face
        for jdof= helmprb.dofbegin(idx(3)):helmprb.dofend(idx(3))-1
            if helmprb.m1(jdof)==helmprb.m1(idof)
                count= count+1;
                ii(count)= idof;
                jj(count)= jdof;
                kz= helmprb.m2(jdof)*pi/h(3);
                s= sqrt((xiele(1)*kx)^2+(xiele(3)*kz)^2-wele^2)/xiele(2);
                %s= sfun(2,kx,kz,xiele,wele);
                vv(count)= Cx*h(1)*rhoele/xiele(2)/(s^2+ky^2);
            end
        end
        %-}

        %-{ non-parallel interaction with back face
        for jdof= helmprb.dofbegin(idx(4)):helmprb.dofend(idx(4))-1
            if helmprb.m1(jdof)==helmprb.m1(idof)
                count= count+1;
                ii(count)= idof;
                jj(count)= jdof;
                kz= helmprb.m2(jdof)*pi/h(3);
                s= sqrt((xiele(1)*kx)^2+(xiele(3)*kz)^2-wele^2)/xiele(2);
                %s= sfun(2,kx,kz,xiele,wele);
                vv(count)= -Cx*h(1)*rhoele*cos(my*pi)/xiele(2)/(s^2+ky^2);
            end
        end
        %-}

        %-{ non-parallel interaction with left face
        for jdof= helmprb.dofbegin(idx(1)):helmprb.dofend(idx(1))-1
            if helmprb.m1(jdof)==helmprb.m2(idof)
                count= count+1;
                ii(count)= idof;
                jj(count)= jdof;
                kz= helmprb.m2(jdof)*pi/h(3);
                s= sqrt((xiele(2)*ky)^2+(xiele(3)*kz)^2-wele^2)/xiele(1);
                %s= sfun(1,ky,kz,xiele,wele);
                vv(count)= Cy*h(2)*rhoele/xiele(1)/(s^2+kx^2);
            end
        end
        %-}

        %-{ non-parallel interaction with right face
        for jdof= helmprb.dofbegin(idx(2)):helmprb.dofend(idx(2))-1
            if helmprb.m1(jdof)==helmprb.m2(idof)
                count= count+1;
                ii(count)= idof;
                jj(count)= jdof;
                kz= helmprb.m2(jdof)*pi/h(3);
                s= sqrt((xiele(2)*ky)^2+(xiele(3)*kz)^2-wele^2)/xiele(1);
                %s= sfun(1,ky,kz,xiele,wele);
                vv(count)= -Cy*h(2)*rhoele*cos(mx*pi)/xiele(1)/(s^2+kx^2);
            end
        end
        %-}
    end
    %-}

    %-{   rows of top face: *test* by top face modes, sign Dirichlet with +
    for idof= helmprb.dofbegin(idx(6)):helmprb.dofend(idx(6))-1
        mx= helmprb.m1(idof);
        my= helmprb.m2(idof);
        kx= mx*pi/h(1);
        ky= my*pi/h(2);
        Cxy= 0.25*(1+(ky==0))*(1+(kx==0));
        Cy= 0.5*(1+(ky==0));
        Cx= 0.5*(1+(kx==0));

        %-{ self interaction
        count= count+1;
        ii(count)= idof;
        jj(count)= idof;
        s= sqrt((xiele(1)*kx)^2+(xiele(2)*ky)^2-wele^2)/xiele(3);
        %s= sfun(3,kx,ky,xiele,wele);
        vv(count)= Cxy*h(1)*h(2)*rhoele/xiele(3)/ ...
            s*(exp((-s*h(3)))+exp(s*h(3)))/(-exp((-s*h(3)))+exp(s*h(3)));
        %-}

        %-{ parallel interaction
        for jdof= helmprb.dofbegin(idx(5)):helmprb.dofend(idx(5))-1
            if helmprb.m1(jdof)==helmprb.m1(idof) && helmprb.m2(jdof)==helmprb.m2(idof)
                count= count+1;
                ii(count)= idof;
                jj(count)= jdof;
                %s= sfun(3,kx,ky,xiele,wele);
                vv(count)= -2.0*Cxy*h(1)*h(2)*rhoele/xiele(3)/s/(-exp((-s*h(3)))+exp(s*h(3)));
                break;
            end
        end
        %-}


        %-{ non-parallel interaction with front face
        for jdof= helmprb.dofbegin(idx(3)):helmprb.dofend(idx(3))-1
            if helmprb.m1(jdof)==helmprb.m1(idof)
                count= count+1;
                ii(count)= idof;
                jj(count)= jdof;
                mz= helmprb.m2(jdof);
                kz= mz*pi/h(3);
                s= sqrt((xiele(1)*kx)^2+(xiele(3)*kz)^2-wele^2)/xiele(2);
                %s= sfun(2,kx,kz,xiele,wele);
                vv(count)= -Cx*h(1)*rhoele*cos(mz*pi)/xiele(2)/(s^2+ky^2);
            end
        end
        %-}

        %-{ non-parallel interaction with back face
        for jdof= helmprb.dofbegin(idx(4)):helmprb.dofend(idx(4))-1
            if helmprb.m1(jdof)==helmprb.m1(idof)
                count= count+1;
                ii(count)= idof;
                jj(count)= jdof;
                mz= helmprb.m2(jdof);
                kz= mz*pi/h(3);
                s= sqrt((xiele(1)*kx)^2+(xiele(3)*kz)^2-wele^2)/xiele(2);
                %s= sfun(2,kx,kz,xiele,wele);
                vv(count)= Cx*h(1)*rhoele*cos(my*pi)*cos(mz*pi)/xiele(2)/(s^2+ky^2);
            end
        end
        %-}

        %-{ non-parallel interaction with left face
        for jdof= helmprb.dofbegin(idx(1)):helmprb.dofend(idx(1))-1
            if helmprb.m1(jdof)==helmprb.m2(idof)
                count= count+1;
                ii(count)= idof;
                jj(count)= jdof;
                mz= helmprb.m2(jdof);
                kz= mz*pi/h(3);
                s= sqrt((xiele(2)*ky)^2+(xiele(3)*kz)^2-wele^2)/xiele(1);
                %s= sfun(1,ky,kz,xiele,wele);
                vv(count)= -Cy*h(2)*rhoele*cos(mz*pi)/xiele(1)/(s^2+kx^2);
            end
        end
        %-}

        %-{ non-parallel interaction with right face
        for jdof= helmprb.dofbegin(idx(2)):helmprb.dofend(idx(2))-1
            if helmprb.m1(jdof)==helmprb.m2(idof)
                count= count+1;
                ii(count)= idof;
                jj(count)= jdof;
                mz= helmprb.m2(jdof);
                kz= mz*pi/h(3);
                s= sqrt((xiele(2)*ky)^2+(xiele(3)*kz)^2-wele^2)/xiele(1);
                %s= sfun(1,ky,kz,xiele,wele);
                vv(count)= Cy*h(2)*rhoele*cos(mx*pi)*cos(mz*pi)/xiele(1)/(s^2+kx^2);
            end
        end
        %-}
    end
    %-}

end


%    create sparse matrix A.' from arrays ii,jj,vv
nrowA= helmprb.dofend(helmprb.tnf)-1;
A= sparse(jj(1:count),ii(1:count),vv(1:count),nrowA,nrowA);
clear ii jj vv;

%    treat Neumann and Robin boundary conditions: Matlab favors column operations

%-{ left face of the domain augmented with pml
if helmprb.bctype(1)<2
    for iy= 1:helmprb.npml(2)
        for iz= 1:helmprb.npml(3)
            idx= sub2idx(1,[1 iy iz],helmprb.npml,helmprb.nf);
            subphy= subpml2phy([1,iy,iz]);
            if helmprb.bctype(1)==1
                pele= helmprb.p(helmprb.w(subphy(1),subphy(2),subphy(3)));
            end
            for idof= helmprb.dofbegin(idx):helmprb.dofend(idx)-1
                my= helmprb.m1(idof);
                mz= helmprb.m2(idof);
                ky= my*pi/h(2);
                kz= mz*pi/h(3);
                Cyz= 0.25*(1+(ky==0))*(1+(kz==0));
                if helmprb.bctype(1)==1
                    jj= find(A(:,idof));                    
                    A(jj,idof)= A(jj,idof)*(pele(1)+pele(2)*(ky^2+kz^2));
                    A(idof,idof)= A(idof,idof)+Cyz*h(2)*h(3);
                elseif helmprb.tgv>1e8
                    A(idof,idof)= A(idof,idof) + helmprb.tgv*Cyz*h(2)*h(3);
                else                    
                    jj= find(A(:,idof));
                    A(jj,idof)= 0;
                    A(idof,idof)= helmprb.tgv*Cyz*h(2)*h(3);
                end
            end
        end        
    end    
end
%-}

%-{ right face of the domain
if helmprb.bctype(2)<2
    for iy= 1:helmprb.npml(2)
        for iz= 1:helmprb.npml(3)
            idx= sub2idx(1,[helmprb.npml(1)+1 iy iz],helmprb.npml,helmprb.nf);
            subphy= subpml2phy([helmprb.npml(1),iy,iz]);
            if helmprb.bctype(2)==1
                pele= helmprb.p(helmprb.w(subphy(1),subphy(2),subphy(3)));
            end
            for idof= helmprb.dofbegin(idx):helmprb.dofend(idx)-1
                my= helmprb.m1(idof);
                mz= helmprb.m2(idof);
                ky= my*pi/h(2);
                kz= mz*pi/h(3);
                Cyz= 0.25*(1+(ky==0))*(1+(kz==0));
                if helmprb.bctype(2)==1
                    jj= find(A(:,idof));
                    A(jj,idof)= A(jj,idof)*(pele(1)+pele(2)*(ky^2+kz^2));
                    A(idof,idof)= A(idof,idof)+Cyz*h(2)*h(3);
                elseif helmprb.tgv>1e8
                    A(idof,idof)= A(idof,idof) + helmprb.tgv*Cyz*h(2)*h(3);
                else                    
                    jj= find(A(:,idof));
                    A(jj,idof)= 0;
                    A(idof,idof)= helmprb.tgv*Cyz*h(2)*h(3);
                end
            end
        end        
    end    
end
%-}


%-{ front face of the domain
if helmprb.bctype(3)<2
    for iz= 1:helmprb.npml(3)
        for ix= 1:helmprb.npml(1)
            idx= sub2idx(2,[ix 1 iz],helmprb.npml,helmprb.nf);
            subphy= subpml2phy([ix,1,iz]);
            if helmprb.bctype(3)==1
                pele= helmprb.p(helmprb.w(subphy(1),subphy(2),subphy(3)));
            end
            for idof= helmprb.dofbegin(idx):helmprb.dofend(idx)-1
                mx= helmprb.m1(idof);
                mz= helmprb.m2(idof);
                kx= mx*pi/h(1);
                kz= mz*pi/h(3);
                Cxz= 0.25*(1+(kx==0))*(1+(kz==0));
                if helmprb.bctype(3)==1
                    jj= find(A(:,idof));
                    A(jj,idof)= A(jj,idof)*(pele(1)+pele(2)*(kx^2+kz^2));
                    A(idof,idof)= A(idof,idof)+Cxz*h(1)*h(3);
                elseif helmprb.tgv>1e8
                    A(idof,idof)= A(idof,idof) + helmprb.tgv*Cxz*h(1)*h(3);
                else                    
                    jj= find(A(:,idof));
                    A(jj,idof)= 0;
                    A(idof,idof)= helmprb.tgv*Cxz*h(1)*h(3);
                end
            end
        end        
    end    
end
%-}

%-{ back face of the domain
if helmprb.bctype(4)<2
    for iz= 1:helmprb.npml(3)
        for ix= 1:helmprb.npml(1)
            idx= sub2idx(2,[ix helmprb.npml(2)+1 iz],helmprb.npml,helmprb.nf);
            subphy= subpml2phy([ix,helmprb.npml(2),iz]);
            if helmprb.bctype(4)==1
                pele= helmprb.p(helmprb.w(subphy(1),subphy(2),subphy(3)));
            end
            for idof= helmprb.dofbegin(idx):helmprb.dofend(idx)-1
                mx= helmprb.m1(idof);
                mz= helmprb.m2(idof);
                kx= mx*pi/h(1);
                kz= mz*pi/h(3);
                Cxz= 0.25*(1+(kx==0))*(1+(kz==0));
                if helmprb.bctype(4)==1
                    jj= find(A(:,idof)); 
                    A(jj,idof)= A(jj,idof)*(pele(1)+pele(2)*(kx^2+kz^2));
                    A(idof,idof)= A(idof,idof)+Cxz*h(1)*h(3);
                elseif helmprb.tgv>1e8
                    A(idof,idof)= A(idof,idof) + helmprb.tgv*Cxz*h(1)*h(3);
                else                    
                    jj= find(A(:,idof));
                    A(jj,idof)= 0;
                    A(idof,idof)= helmprb.tgv*Cxz*h(1)*h(3);
                end
            end
        end        
    end    
end
%-}


%-{ bottom face of the domain
if helmprb.bctype(5)<2
    for ix= 1:helmprb.npml(1)
        for iy= 1:helmprb.npml(2)
            idx= sub2idx(3,[ix iy 1],helmprb.npml,helmprb.nf);
            subphy= subpml2phy([ix,iy,1]);
            if helmprb.bctype(5)==1
                pele= helmprb.p(helmprb.w(subphy(1),subphy(2),subphy(3)));
            end
            for idof= helmprb.dofbegin(idx):helmprb.dofend(idx)-1
                mx= helmprb.m1(idof);
                my= helmprb.m2(idof);
                kx= mx*pi/h(1);
                ky= my*pi/h(2);
                Cxy= 0.25*(1+(kx==0))*(1+(ky==0));
                if helmprb.bctype(5)==1
                    jj= find(A(:,idof));
                    A(jj,idof)= A(jj,idof)*(pele(1)+pele(2)*(kx^2+ky^2));
                    A(idof,idof)= A(idof,idof)+Cxy*h(1)*h(2);
                elseif helmprb.tgv>1e8
                    A(idof,idof)= A(idof,idof) + helmprb.tgv*Cxy*h(1)*h(2);
                else                    
                    jj= find(A(:,idof));
                    A(jj,idof)= 0;
                    A(idof,idof)= helmprb.tgv*Cxy*h(1)*h(2);
                end
            end
        end        
    end    
end
%-}

%-{ top face of the domain
if helmprb.bctype(6)<2
    for ix= 1:helmprb.npml(1)
        for iy= 1:helmprb.npml(2)
            idx= sub2idx(3,[ix iy 1+helmprb.npml(3)],helmprb.npml,helmprb.nf);
            subphy= subpml2phy([ix,iy,helmprb.npml(3)]);
            if helmprb.bctype(6)==1
                pele= helmprb.p(helmprb.w(subphy(1),subphy(2),subphy(3)));
            end
            for idof= helmprb.dofbegin(idx):helmprb.dofend(idx)-1
                mx= helmprb.m1(idof);
                my= helmprb.m2(idof);
                kx= mx*pi/h(1);
                ky= my*pi/h(2);
                Cxy= 0.25*(1+(kx==0))*(1+(ky==0));
                if helmprb.bctype(6)==1
                    jj= find(A(:,idof));
                    A(jj,idof)= A(jj,idof)*(pele(1)+pele(2)*(kx^2+ky^2));
                    A(idof,idof)= A(idof,idof)+Cxy*h(1)*h(2);
                elseif helmprb.tgv>1e8
                    A(idof,idof)= A(idof,idof) + helmprb.tgv*Cxy*h(1)*h(2);
                else                    
                    jj= find(A(:,idof));
                    A(jj,idof)= 0;
                    A(idof,idof)= helmprb.tgv*Cxy*h(1)*h(2);
                end
            end
        end        
    end    
end
%-}



%    transpose A
A= A.';

%    end of this function
end