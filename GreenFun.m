function re = GreenFun(scpoints,x,y,z,w,sgn)
%GreenFun -- Evaluate the Green function of the Helmholtz equation generated
%by point sources in the free space R^3.
%
%   re = GreenFun(scpoints,x,y,z,w,sgn)

%   initialization
re= zeros(size(x));
distx= re;

%   loop over every source point
for isrc= 1:size(scpoints,1)
    x0= scpoints(isrc,1);
    y0= scpoints(isrc,2);
    z0= scpoints(isrc,3);
    distx= sqrt((x-x0).^2+(y-y0).^2+(z-z0).^2);
    re= re + exp(-sgn*1i*w*distx)./distx/4.0/pi;
end
